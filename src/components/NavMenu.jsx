import React from "react";
import { Nav, Navbar, Button, FormControl, Form, Container  } from "react-bootstrap";

function NavMenu() {
  return (
    <Navbar bg="success" variant="dark">
      <Container>
    <Navbar.Brand href="#home">KFC</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="#home">Home</Nav.Link>
      <Nav.Link href="#features">Features</Nav.Link>
      <Nav.Link href="#pricing">Pricing</Nav.Link>
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-light">Search</Button>
    </Form>
    </Container>
  </Navbar>
  );
}

export default NavMenu;
