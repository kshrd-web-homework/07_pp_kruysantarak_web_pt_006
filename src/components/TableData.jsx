import React from "react";
import { Container, Table, Form, Button } from "react-bootstrap";

let subTotal = (temp) => {
  let arrTotal = temp.map((item) => item.total);
  return arrTotal.reduce((pre, cur) => pre + cur, 0);
};

let totalAmount = (subTotal, taxInclude) => {
  return subTotal * (1 + taxInclude / 100);
};

let taxInclude = (temp) => {
  let tax = 0;
  if (temp.length === 0) {
    tax = 0;
  } else {
    tax = 10;
  }
  return tax;
};

let mySet = new Set();

function TableData(props) {
  let items = [...props.items];
  items.filter((item) => {
    if (item.amount > 0) {
      mySet.add(item);
    } else {
      mySet.delete(item);
    }
  });

  let temp = Array.from(mySet).filter((item) => {
    return item.amount > 0;
  });

  // let temp = items.filter((item, index, self) => {
  //   return item.amount > 0;
  // });

  let formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return (
    <Container>
      <Button
        variant="warning"
        className="mt-3 mb-3 mr-2"
        onClick={props.onClear}
      >
        Clear All
      </Button>{" "}
      <Button variant="success" className="mt-3 mb-3 mr-2">
        {temp.length}
      </Button>{" "}
      <Table striped borderless hover>
        <thead>
          <tr style={{ backgroundColor: "#4ca1a3", color: "#fff" }}>
            <th style={{ width: "10%" }}>#</th>
            <th style={{ width: "25%" }}>Discription</th>
            <th style={{ width: "15%" }}>Quantity</th>
            <th style={{ width: "15%" }}>Unit Price</th>
            <th style={{ width: "15%" }}>Discount</th>
            <th style={{ width: "20%" }}>Total</th>
          </tr>
        </thead>
        <tbody>
          {temp.map((item, index) => (
            <tr>
              <td>{index + 1}</td>
              <td>{item.title}</td>
              <td>{item.amount}</td>
              <td>{formatter.format(item.price)}</td>
              <td>% {Number(item.discount).toFixed(2)}</td>
              <td>{formatter.format(item.total)}</td>
            </tr>
          ))}
          <tr style={{ backgroundColor: "white" }}>
            <td colSpan={4}></td>
          </tr>
          <tr style={{ backgroundColor: "white" }}>
            <td colSpan={4} style={{ border: "0px" }}></td>
            <td style={{ fontWeight: "bold" }}>Sub Total</td>
            <td style={{ color: "#5b6d5b", fontWeight: "bold" }}>
              {formatter.format(subTotal(temp))}
            </td>
          </tr>
          <tr style={{ backgroundColor: "white" }}>
            <td colSpan={4}></td>
            <td style={{ fontWeight: "bold" }}>Tax Include</td>
            <td style={{ color: "#f55c47", fontWeight: "bold" }}>
              %{Number(taxInclude(temp)).toFixed(2)}
            </td>
          </tr>
          <tr style={{ backgroundColor: "white" }}>
            <td colSpan={4}></td>
            <td style={{ fontWeight: "bold" }}>Total Amount</td>
            <td style={{ color: "#4aa96c", fontWeight: "bold" }}>
              {formatter.format(totalAmount(subTotal(temp), taxInclude(temp)))}
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
}

export default TableData;
