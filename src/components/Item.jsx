import React from "react";
import { Card, Button, Badge } from "react-bootstrap";

let formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

function Item(props) {
  return (
    <Card key={props.id}>
      <Card.Img variant="top" src={props.item.img} />
      <Card.Body>
        <Card.Title>
          <h3>{props.item.title}</h3>
        </Card.Title>
        <Card.Text style={{ fontWeight: "bold" }}>
          Price: {formatter.format(props.item.price)}{" "}
        </Card.Text>

        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
          }}
          className="mb-2 mr-2"
        >
          <Button
            style={{ width: "100%" }}
            variant="success"
            onClick={() => props.onAdd(props.idx)}
          >
            Add to cart{" "}
            <Badge className="ml-2" variant="warning">
              {props.item.amount}
            </Badge>
            <span className="sr-only">unread messages</span>
          </Button>
        </div>
        <div>
          <Button
            style={{ width: "100%" }}
            variant="danger"
            disabled={props.item.amount === 0}
            onClick={() => {
              props.onDelete(props.idx);
            }}
          >
            Remove
          </Button>
        </div>

        <h5 style={{ fontWeight: "bold" }} className="my-2">
          Total: {formatter.format(props.item.total)}{" "}
        </h5>
      </Card.Body>
    </Card>
  );
}

export default Item;
